"use strict"

//1. Існує 3 варіанти задати змінну - const, let та var. Остання вважається застарілою.
//2. Різниця між prompt та confirm в тому, що в prompt можна ввести якусь інформацію, а confirm не має поля для вводу.
//3. Неявне перетворення типів - це зміна типу даних без безпосередньої дії на її зміну. Тобто тип даних змінюється побічно. Наприклад при спробі складання(арифметичного) строкового значення з чисельним значенням чисельне значення стає строковим. 

let name = 'Andrii';
let admin;

admin = name;
console.log(admin);

let days = 8;
let seconds = (days*24*60*60);
console.log(seconds);

let result = prompt("Enter the message");
console.log(result);